<?php

//This is only for slavic languages at the moment as their spelling changes in certain cases requiring us to add an additional "plural".

$l['year2'] = 'Lata'; 
$l['months2'] = 'Miesiace'; 
$l['days2'] = 'Dni';
$l['weeks2'] = 'Tygodnie';
$l['hours2'] = 'Godziny';
$l['minutes2'] = 'minuty';
$l['seconds2'] = 'sekundy';

<?php
$l['restore'] = "Przywróć";
$l['move_to_trash'] = "Przenieś Do Śmieci";
$l['compose'] = "Komponuj";
$l['trash'] = "śmieci";
$l['inbox'] = "Inbox";
$l['sent_messages'] = "Wysłano Wiadomość";
$l['receiver'] = "Odbiorca";
$l['sender'] = "Wysyła";
$l['actions'] = "Akcje";
$l['subject'] = "Tytuł";
$l['message'] = "Waidomość";
$l['to'] = "Do";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "Nie Znaleziono Wiadomości";
$l['no_more_messages_found'] = "Nie można było Znaleśc Więcej Wiadomości";
$l['messages'] = "Wiadomości";
$l['send_message_banned'] = "Nie Możesz Wysłać Wiadomości Kiedy Jesteś Zbanowany.";

<?php

$l['mydownloads_being_updated'] = 'aktualizowanie';
$l['mydownloads_meta_by'] = '{1} przez {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'Nie możesz oceniać swoich modyfikacji.';
$l['mydownloads_collaborators_desc'] = 'Współałtorzy to osoby które mogą zmieniać strone mopdyfikacji. Oni Nie mogą Usunąć Modyfikacji i Dodawać Współałtorów';
$l['mydownloads_comment_banned'] = "Nie Możesz Ddoać Komentarza Kiedy Jesteś Zbanowany.";
$l['mydownloads_delete_confirm'] = "JesteśPewien że Chcesz Usunąć ten Komentarz?";
$l['mydownloads_download_description'] = "Opis";
$l['mydownloads_download_is_suspended'] = '
Ta Modyfikacja Jest Zawieszona i Może Być Oglądana TYLKO przez Kreatopra Modyfikacji I Administracje.
<br/>Te Zawieszenie Może Być Tylko Czasowe lub Na Zawsze Ponieważ Naruszyło Zasade <a style="text-decoration:underline;" href="/rules">rules</a>.
<br/>Jeśli chcesz Odblokować Swoją Modyfikacje Proszę Wejdź w "Odblokuj modyfikacje" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">here</a>.';
$l['mydownloads_download_changelog'] = 'Changelog';
$l['mydownloads_download'] = "Pobranie";
$l['mydownloads_license'] = 'Licencja';
$l['mydownloads_report_download'] = 'Zkłoś Modyfikacje';
$l['show_download_link_warn'] = 'Uważaj Na Podejrzane linki. Jeśli Myslisz Że To wirus Możesz Zgłosić Tą Modyfikacje';
$l['show_download_link'] = 'Pokaż Link Do Pobrania';
$l['show_files'] = 'Pokaż Pliki';
$l['submitted_by'] = 'Przesłane Przez';
$l['subscribe_commnet_help'] = 'Dostań Powiadomienie Kiedy Ktoś Napisze Komentarz';
$l['follow_mod_help'] = 'Obserwuj Modyfikacje Żeby Dostać Powiadomienie Kiedy Dostanie Aktualizacje';
$l['mydownloads_download_comments'] = "Komentarze";
$l['mydownloads_unsuspend_it'] = 'Cofnij Zawieszenie';
$l['mydownloads_suspend_it'] = 'Zawieś';
$l['mydownloads_files_alert'] = '<strong>NO FILES</strong> | Ponieważ Ta Modyfikacja Nie ma Plików Jest Ona Niewidzialna.';
$l['mydownloads_files_alert_waiting'] = '<strong>NO APPROVED FILES</strong> | Ta Modyfikacja Jest Nieiwdzialna Dopuki Nie Zostanie Potwierdzona.';
$l['share'] = 'Podziel Się';

<?php
$l['restore'] = "恢复";
$l['move_to_trash'] = "移至垃圾桶";
$l['compose'] = "编写私信";
$l['trash'] = "垃圾箱";
$l['inbox'] = "收件箱";
$l['sent_messages'] = "已经发送的消息";
$l['receiver'] = "接受者";
$l['sender'] = "发送者";
$l['actions'] = "行动";
$l['subject'] = "主题";
$l['message'] = "消息";
$l['to'] = "收件人";
$l['BCC'] = "暗抄送（密送）";
$l['no_messages_found'] = "没有新的消息了QwQ";
$l['no_more_messages_found'] = "怎么找都找不到新的消息了 TAT";
$l['messages'] = "消息";
$l['send_message_banned'] = "您被封禁了！无法发送消息，请向管理员询问原因吧(u_u)";
